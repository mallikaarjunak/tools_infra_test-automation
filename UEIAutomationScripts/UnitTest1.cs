﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;

namespace UEIAutomationScripts
{
    [TestClass]
    public class UnitTest1
    {
        IWebDriver driverOne = new FirefoxDriver();
      //  [TestMethod]
        public void LaunchURL()
        {
            //Open URL
            driverOne.Navigate().GoToUrl("https://citstuei2edit.azurewebsites.net");
            Thread.Sleep(10000);
         }
        
        public void UEIlogin(string Username,String Password )
        {
            //Enter UserName
            driverOne.FindElement(By.Id("UserName")).SendKeys(Username);
            //Enter Password
            driverOne.FindElement(By.Id("Password")).SendKeys(Password);
            //Enter click and login button
            //string buttonname = driverOne.FindElement(By.XPath(".//input[@class='btn btn-primary']")).GetAttribute("value");
            driverOne.FindElement(By.XPath(".//input[@class='btn btn-primary']")).Click();
           
        }

        public string TN_ImportCapture()
        {
            bool isfound = false;
            //click on TN
            driverOne.FindElement(By.XPath(".//div[@id='navbar']/ul/li[3]")).Click();
            IWebElement Dropdownlist = driverOne.FindElement(By.XPath(".//*[@id='navbar']/ul/li[3]/ul"));
            IList<IWebElement> dropdown = Dropdownlist.FindElements(By.XPath("li"));
            int count1 = dropdown.Count;
            for (int i = 0; i < count1 && !isfound; i++)
            {
                if (dropdown[i].Text == "Import Capture (New TN)")
                {
                    dropdown[i].Click();
                    isfound = true;
                }
            }

            Thread.Sleep(12000);


            //Upload the file
            string FilePath = @"E:\T-cello.zip";
            driverOne.FindElement(By.Id("fuCaptureFile")).Click();

            //Click(AttributeName, attributeValue);
            Thread.Sleep(10000);
            System.Windows.Forms.SendKeys.SendWait(FilePath);

            Thread.Sleep(9000);
            System.Windows.Forms.SendKeys.SendWait(@"{Enter}");
            driverOne.FindElement(By.Id("btnImportCapture")).Click();
            Thread.Sleep(15000);

            //File upload sucess msg xclose
            driverOne.FindElement(By.XPath(".//*[@id='ActionMsg-Success']/div/div/div[1]/table/tbody/tr/td[2]/button")).Click();
            Thread.Sleep(10000);

            //Click on devicedropdown
            driverOne.FindElement(By.Id("select2-MainDeviceModel_SelectedValue-container")).Click();

            //Search device
            driverOne.FindElement(By.XPath(".//input[@class='select2-search__field']")).SendKeys("DVD");

            //Select DVD
            SelectByVisibleTextFromDropDown(".//span[@class='select2-results']/ul", "DVD");
            Thread.Sleep(9000);

            //Click on Mode drop down
            driverOne.FindElement(By.Id("select2-ModeModel_SelectedValue-container")).Click();

            //Search box for mode drop down
            driverOne.FindElement(By.XPath(".//input[@class='select2-search__field']")).SendKeys("Y");

            //Click on Y
            SelectByVisibleTextFromDropDown(".//span[@class='select2-results']/ul", "Y");
            Thread.Sleep(9000);

            //Click on Brand drop down
            driverOne.FindElement(By.Id("select2-Brand-container")).Click();

            //Search box for Brand drop down
            driverOne.FindElement(By.XPath(".//input[@class='select2-search__field']")).SendKeys("Samsung");

            //Click on Samsung
            SelectByVisibleTextFromDropDown(".//span[@class='select2-results']/ul", "Samsung");
            Thread.Sleep(6000);

            //Add comment
            driverOne.FindElement(By.Id("ModelName")).SendKeys("DemoModel");

            //Add comment
            driverOne.FindElement(By.Id("Comment")).SendKeys("TestTN");

            //Click on AssignTn button
            driverOne.FindElement(By.Id("btnAssignTN")).Click();
            Thread.Sleep(15000);

            //Capture the Tn number
            string CaturedText = driverOne.FindElement(By.XPath(".//*[@id='ActionMsg-Success']/div/div/div[2]/div")).Text;
            string[] splitTNNumber = CaturedText.Split(':');
            string TNnumber = splitTNNumber[1].Trim();
            Console.WriteLine("New Tn number : " + TNnumber);

            //Close button from Tn number pop up
            driverOne.FindElement(By.XPath(".//*[@id='ActionMsg-Success']/div/div/div[1]/table/tbody/tr/td[2]/button")).Click();
            Thread.Sleep(10000);
            return TNnumber;
        }
        public void TN_EditPage(string TNnumber)
        {
            Thread.Sleep(10000);

            //Selecting TN -Edit Mode
            driverOne.FindElement(By.XPath(".//div[@id='navbar']/ul/li[3]")).Click();
            SelectByVisibleTextFromDropDown(".//*[@id='navbar']/ul/li[3]/ul", "Edit");
            Thread.Sleep(9000);

            //Expand My TN Check out Sessions
            driverOne.FindElement(By.Id("btnMyCheckedOutTns")).Click();
            Thread.Sleep(6000);
            List<string> TNnumberListFromUI = new List<string>();
            bool flag = false;
            IList<IWebElement> TnList = driverOne.FindElements(By.XPath(".//*[@id='divEditingTnListPartial']/table/tbody/tr[3]/td[2]/table/tbody/tr/td[1]"));
            foreach (var r in TnList)
            {
                TNnumberListFromUI.Add(r.Text);
            }
            foreach (string s in TNnumberListFromUI)
            {
                if (s.Equals(TNnumber))
                {
                    flag = true;
                }
            }
            Assert.IsTrue(flag);
            Console.WriteLine("Created New TN Number Cross Verified Successfully And The New TN Is :" + TNnumber);
        }


        public void SelectByVisibleTextFromDropDown(string Dropdown, string SelectText)
        {
            bool isfound = false;
            IWebElement Dropdownlist = driverOne.FindElement(By.XPath(Dropdown));
            IList<IWebElement> dropdown = Dropdownlist.FindElements(By.XPath("li"));
            int count1 = dropdown.Count;
            for (int i = 0; i < count1 && !isfound; i++)
            {
                if (dropdown[i].Text == SelectText)
                {
                    dropdown[i].Click();
                    isfound = true;
                }
            }
        }

        public void Browserclose()
        {
            driverOne.Close();
        }

    }
}
