﻿using System;
using TechTalk.SpecFlow;

using UEIAutomationScripts;
namespace UEIAutomationScripts.TestScripts
{
    [Binding]
    public class TNNumberValidationsSteps
    {
        string Tnnmber;
        UnitTest1 u1 = new UnitTest1();

        [Given(@"I am user of UEI(.*)Edit")]
        public void GivenIAmUserOfUEIEdit(int p0)
        {
              u1.LaunchURL();
        }

        [Given(@"Logged in to UEI(.*)Edit")]
        public void GivenLoggedInToUEIEdit(int p0)
        {
             u1.UEIlogin("kmallikarjuna", "{$^V~W6rV=Kt9XHj");
        }

        [When(@"I click import new remote capture file and generate the New TN")]
        public void WhenIClickImportNewRemoteCaptureFileAndGenerateTheNewTN()
        {
            Tnnmber = u1.TN_ImportCapture();
        }

        [Then(@"I should see newly generated Tn in the Edit Page")]
        public void ThenIShouldSeeNewlyGeneratedTnInTheEditPage()
        {
            u1.TN_EditPage(Tnnmber);
            u1.Browserclose();
        }
    }
}
